package i18n

const (
	Success             = "common.success"
	Failed              = "common.failed"
	UpdateSuccess       = "common.updateSuccess"
	UpdateFailed        = "common.updateFailed"
	CreateSuccess       = "common.createSuccess"
	CreateFailed        = "common.createFailed"
	DeleteSuccess       = "common.deleteSuccess"
	DeleteFailed        = "common.deleteFailed"
	ChangeStatusSuccess = "common.changeStatusSuccess"
	ChangeStatusFailed  = "common.changeStatusFailed"
	TargetNotFound      = "common.targetNotExist"
	DatabaseError       = "common.databaseError"
	RedisError          = "common.redisError"
)
